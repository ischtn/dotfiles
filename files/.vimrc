" Visual settings
syntax on
colorscheme default
set viminfo=""
set number

" Convert tabs to spaces and set width of tabs
set tabstop=4
set shiftwidth=4
set expandtab

" Menu options
set wildmenu
set is
set hls
set history=1000
set showcmd

" Keep some context, i.e. when centering with z
set scrolloff=5

" Show search results while typing, be smart on casing.
set incsearch
set ignorecase
set smartcase
set lbr

" Share clipboard with vim
set clipboard=unnamed

" Set auto and smart indentation
set ai
set si

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

imap jk <Esc>
