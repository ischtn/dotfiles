# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH
export LANG=en_US.UTF-8
export PATH="/usr/local/sbin:$PATH"

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="agnoster"
#ZSH_THEME="robbyrussell"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  docker
  django
  python
  zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh

################################################################################
# User configuration
################################################################################

DEFAULT_USER="Igor $prompt_contest(){}"

if type brew &>/dev/null; then
  FPATH=$(brew --prefix)/share/zsh/site-functions:$FPATH

  autoload -Uz compinit
  compinit
fi

# Python, pyenv stuff
# Create virtual environments from pipenv in project folder instead ~/.virtualenvs/
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"

# For pipenv
export PIPENV_VENV_IN_PROJECT="true"
export PIPENV_VERBOSITY=-1
export PATH="$HOME/.local/bin:$PATH"

# Set the tcl flags for compilers to use the brew tcl-tk version rather than mac os.
# Compilation flags
# export ARCHFLAGS="-arch x86_64"
export LDFLAGS="-L/usr/local/opt/tcl-tk/lib"
export CPPFLAGS="-I/usr/local/opt/tcl-tk/include"

# unset LSCOLORS
export CLICOLOR=1
export CLICOLOR_FORCE=1
