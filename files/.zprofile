alias python2='/Users/Igor/.pyenv/versions/2.7.16/bin/python'
#alias python='/Users/Igor/.pyenv/versions/3.8.2/bin/python'
alias hidden-hide='defaults write com.apple.finder AppleShowAllFiles FALSE;killall Finder'
alias hidden-show='defaults write com.apple.finder AppleShowAllFiles TRUE;killall Finder'

alias myip='dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | sed s/\"//g'
alias ls='ls -GFh --color=always'
export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8

export PATH="/usr/local/sbin:$PATH"

# Use GNU core utils instead of darwin OSX utilities
PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"

# Use GNU grep instead of BSD grep
PATH="/usr/local/opt/grep/libexec/gnubin:$PATH"

if [ -r ~/.zlocal ]; then
    source ~/.zlocal;
else
    echo "No local aliases in .zlocal found"
fi

